package com.mtt.universal.providers.mutabaah;

/**
 * Created by airgalon on 5/14/2017.
 */

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.mtt.universal.R;
import com.mtt.universal.model.ActivityItem;
import com.mtt.universal.model.PeriodActivityItem;
import com.mtt.universal.providers.adapter.MutabaahHistoryAdapter;

import java.util.ArrayList;

import static com.mtt.universal.MainActivity.FRAGMENT_DATA;
import static com.mtt.universal.MainActivity.mDbHelper;

/**
 * This activity is used to display a list of rss items
 */

public class MutabaahHistoryFragment extends Fragment {

    private static Activity mAct;
    private LinearLayout ll;
    private String url;
    private static MutabaahHistoryAdapter adapter;
    private static ListView list;
    private static ArrayList<String> listPeriod = new ArrayList<String>();
    private static ArrayList<ActivityItem> listActivity = new ArrayList<ActivityItem>();
    private static ArrayList<PeriodActivityItem> listHistory = new ArrayList<PeriodActivityItem>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mAct = getActivity();
        url = MutabaahHistoryFragment.this.getArguments().getStringArray(FRAGMENT_DATA)[0];

        ll = (LinearLayout) inflater.inflate(R.layout.fragment_mutabaah_history, container, false);
        list = (ListView) ll.findViewById(R.id.listView1);

        getHistory();

        return ll;
    }

    public static void getHistory() {
        listPeriod.clear();
        listActivity.clear();
        listHistory.clear();

        listActivity = mDbHelper.getAllActivity();
        // get all period
        for (int i = 0; i < listActivity.size(); i++) {
            if (i == 0)
                listPeriod.add(listActivity.get(i).getPeriod());
            else {
                if (!listActivity.get(i - 1).getPeriod().equals(listActivity.get(i).getPeriod()))
                    listPeriod.add(listActivity.get(i).getPeriod());
            }
        }
        // get all history based on listPeriod and listActivity
        for (String period : listPeriod) {
            PeriodActivityItem history = new PeriodActivityItem();
            int count = 0;
            history.setPeriod(period);

            for (ActivityItem item : listActivity) {
                if (item.getPeriod().equals(period)) {
                    history.getItems().add(item);
                    count++;
                }
            }
            history.setTotal(count);
            listHistory.add(history);
        }

        adapter = new MutabaahHistoryAdapter(mAct, listHistory);
        list.setAdapter(adapter);
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            getHistory();
        }
    }


}
