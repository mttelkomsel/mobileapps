package com.mtt.universal.providers.mutabaah;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mtt.universal.R;
import com.mtt.universal.model.ActivityItem;
import com.mtt.universal.model.PeriodActivityItem;
import com.mtt.universal.providers.adapter.MutabaahHistoryDetailAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.mtt.universal.MainActivity.listHistoryChart;
import static com.mtt.universal.MainActivity.mDbHelper;
import static com.mtt.universal.MainActivity.myAlert;
import static com.mtt.universal.MainActivity.mChart;
import static com.mtt.universal.providers.mutabaah.MutabaahHistoryFragment.getHistory;

/**
 * Created by emirdon on 5/20/2017.
 */

public class MutabaahAlert {
    private Context mAct;

    public MutabaahAlert(Context mAct) {
        this.mAct = mAct;
    }

    public void showAlertAddActivity(final ArrayList<LinearLayout> listUserDefined, final LinearLayout llItem) {
        LayoutInflater inflater = LayoutInflater.from(mAct);
        View dialogview = inflater.inflate(R.layout.alertdialog_add_activity,
                null);
        final AlertDialog alert = new AlertDialog.Builder(mAct)
                .create();
        alert.setTitle("Add Activity");
        alert.setView(dialogview);

        final EditText etJudul = (EditText) dialogview.findViewById(R.id.editText1);
        final Spinner sJenis = (Spinner) dialogview
                .findViewById(R.id.spinner1);
        Button bTambah = (Button) dialogview.findViewById(R.id.button1);

        bTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mAct, etJudul.getText().toString()+"\n"+sJenis.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                LayoutInflater inflater = LayoutInflater.from(mAct);
                LinearLayout newLayout = null;

                if (sJenis.getSelectedItem().toString().equals("TextBox")) {
                    newLayout = (LinearLayout) inflater.inflate(R.layout.item_mutabaah_edittext, null, false);
                } else if (sJenis.getSelectedItem().toString().equals("Ceklist")) {
                    newLayout = (LinearLayout) inflater.inflate(R.layout.item_mutabaah_checkbox, null, false);
                }

                TextView tvJudul = (TextView) newLayout.findViewById(R.id.item1);
                tvJudul.setText(etJudul.getText().toString());

                listUserDefined.add(newLayout);
                llItem.addView(newLayout);


                alert.dismiss();
            }
        });

        alert.show();
    }

    public void showAlertHistoryOption(final PeriodActivityItem history) {
        LayoutInflater inflater = LayoutInflater.from(mAct);
        View dialogview = inflater.inflate(R.layout.alertdialog_history_option,
                null);
        final AlertDialog alert = new AlertDialog.Builder(mAct)
                .create();
        //alert.setTitle("Tambah Aktivitas");
        alert.setView(dialogview);

        final Button bView = (Button) dialogview.findViewById(R.id.button1);
        final Button bEdit = (Button) dialogview.findViewById(R.id.button2);
        final Button bDelete = (Button) dialogview.findViewById(R.id.button3);


        bView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAlert.showAlertDetailHistory(history);

                alert.dismiss();
            }
        });

        bEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAlert.showAlertEditHistory(history);

                alert.dismiss();
                if(mChart.getData()!=null) {
                    mChart.getData().notifyDataChanged();
                    mChart.notifyDataSetChanged();
                }
                mChart.invalidate();
            }
        });

        bDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDbHelper.deleteActivity(mAct, history.getPeriod(), false);
                getHistory();

                alert.dismiss();
                if(mChart!=null){
                    if(mChart.getData()!=null) {
                        mChart.getData().notifyDataChanged();
                        mChart.notifyDataSetChanged();
                    }
                    mChart.invalidate();
                }
            }
        });

        alert.show();
    }

    public void showAlertEditHistory(final PeriodActivityItem history) {
        LayoutInflater inflater = LayoutInflater.from(mAct);
        View dialogview = inflater.inflate(R.layout.alertdialog_history_edit,
                null);
        final AlertDialog alert = new AlertDialog.Builder(mAct)
                .create();
        alert.setTitle("Edit Activity");
        alert.setView(dialogview);

        Button bSave = (Button) dialogview.findViewById(R.id.button1);
        LinearLayout llItem = (LinearLayout) dialogview.findViewById(R.id.linearLayout1);
        final ArrayList<LinearLayout> listUserDefined = new ArrayList<LinearLayout>();

        for (ActivityItem activity : history.getItems()) {
            LinearLayout newLayout = null;

            if (activity.getType().equals("editText")) {
                newLayout = (LinearLayout) inflater.inflate(R.layout.item_mutabaah_edittext, null, false);
            } else if (activity.getType().equals("checkBox")) {
                newLayout = (LinearLayout) inflater.inflate(R.layout.item_mutabaah_checkbox, null, false);
            } else if (activity.getType().equals("spinner")) {
                newLayout = (LinearLayout) inflater.inflate(R.layout.item_mutabaah_spinner, null, false);
            }

            TextView tvTitle = (TextView) newLayout.findViewById(R.id.item1);
            tvTitle.setText(activity.getTitle());

            View viewType = newLayout.findViewById(R.id.item2);
            if (viewType instanceof CheckBox) {
                ((CheckBox) viewType).setChecked(activity.getResult().equals("1") ? true : false);
            } else if (viewType instanceof EditText) {
                ((EditText) viewType).setText(activity.getResult());
            } else if (viewType instanceof Spinner) {
                try {
                    int getSelected = 0;
                    if(!activity.getResult().replaceAll("[\\D]","").equals(""))
                    {
                        getSelected = Integer.parseInt(activity.getResult().replaceAll("[\\D]", ""));
                    }

                    ((Spinner) viewType).setSelection(getSelected - 1);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            listUserDefined.add(newLayout);
            llItem.addView(newLayout);
        }

        bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editData(listUserDefined, history.getItems().get(0));
                getHistory();

                alert.dismiss();
            }
        });

        alert.show();
    }

    private void editData(ArrayList<LinearLayout> listUserDefined, ActivityItem item) {

        Calendar now = Calendar.getInstance();
        ActivityItem activity = null;
        ArrayList<ActivityItem> activites = new ArrayList<ActivityItem>();
        SimpleDateFormat datetimeFormatted = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);

        for (LinearLayout layout : listUserDefined) {
            String title = ((TextView) layout.findViewById(R.id.item1)).getText().toString();
            String type = "";
            String result = "";

            View viewType = layout.findViewById(R.id.item2);
            if (viewType instanceof CheckBox) {
                type = "checkBox";
                result = ((CheckBox) viewType).isChecked() ? "1" : "0";
            } else if (viewType instanceof EditText) {
                type = "editText";
                result = ((EditText) viewType).getText().toString();
            } else if (viewType instanceof Spinner) {
                type = "spinner";
                result = ((Spinner) viewType).getSelectedItem().toString();
            }

            activity = new ActivityItem(
                    item.getPeriod(),
                    title,
                    result,
                    type,
                    item.getCreatedTime(),
                    datetimeFormatted.format(now.getTime())
            );
            activites.add(activity);

        }

        mDbHelper.insertActivities(mAct, activites);


    }

    public void showAlertDetailHistory(PeriodActivityItem history) {
        LayoutInflater inflater = LayoutInflater.from(mAct);
        View dialogview = inflater.inflate(R.layout.alertdialog_history_detail,
                null);
        final AlertDialog alert = new AlertDialog.Builder(mAct)
                .create();

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Date date = null;
        try {
            date = formatter.parse(history.getPeriod());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        formatter = new SimpleDateFormat("EEEE, dd MMM yyyy", Locale.US);

        alert.setTitle(formatter.format(date));
        alert.setView(dialogview);

        ListView list = (ListView) dialogview.findViewById(R.id.listView1);
        MutabaahHistoryDetailAdapter adapter = new MutabaahHistoryDetailAdapter(mAct, history.getItems());
        list.setAdapter(adapter);

        alert.show();
    }
}
