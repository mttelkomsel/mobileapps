package com.mtt.universal.providers.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mtt.universal.R;
import com.mtt.universal.model.PeriodActivityItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.mtt.universal.MainActivity.myAlert;


public class MutabaahHistoryAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<PeriodActivityItem> PeriodActivityItems;
    private List<PeriodActivityItem> OriPeriodActivityItems;

    private Filter planetFilter;

    public MutabaahHistoryAdapter(Context context, List<PeriodActivityItem> PeriodActivityItems) {
        this.context = context;
        this.PeriodActivityItems = PeriodActivityItems;
        this.OriPeriodActivityItems = PeriodActivityItems;
    }

    public void resetData() {
        this.PeriodActivityItems = this.OriPeriodActivityItems;
    }

    public int getCount() {
        return PeriodActivityItems.size();
    }

    public PeriodActivityItem getItem(int position) {
        return PeriodActivityItems.get(position);
    }

    private class ViewHolder {

        TextView tvTitle, tvTotal;
        LinearLayout ll;
        //ImageView ivIcon;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;


        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater
                    .inflate(R.layout.layout_mutabaahhistory, parent, false);

            viewHolder = new ViewHolder();

            viewHolder.tvTitle = (TextView) convertView
                    .findViewById(R.id.textView1);
            viewHolder.tvTotal = (TextView) convertView
                    .findViewById(R.id.textView2);
            viewHolder.ll = (LinearLayout) convertView.findViewById(R.id.linearLayout1);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final PeriodActivityItem row = PeriodActivityItems.get(position);

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Date date = null;
        try {
            date = formatter.parse(row.getPeriod());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        formatter = new SimpleDateFormat("EEEE, dd MMM yyyy", Locale.US);
        viewHolder.tvTitle.setText(formatter.format(date));
        viewHolder.tvTotal.setText(Integer.toString(row.getTotal()) + " item(s)");

        viewHolder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAlert.showAlertHistoryOption(row);
            }
        });


//
//        viewHolder.ivIcon.setImageResource(row.getImg());

        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return PeriodActivityItems.indexOf(getItem(position));
    }

    @Override
    public Filter getFilter() {
        if (planetFilter == null)
            planetFilter = new PlanetFilter();

        return planetFilter;
    }

    private class PlanetFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
//            if (constraint == null || constraint.length() == 0) {
//                // No filter implemented we return all the list
//                results.values = OriPeriodActivityItems;
//                results.count = OriPeriodActivityItems.size();
//            } else {
//                // We perform filtering operation
//                List<PeriodActivityItem> nPlanetList = new ArrayList<PeriodActivityItem>();
//
//                for (int i = 0; i < PeriodActivityItems.size(); i++) {
//                    if (PeriodActivityItems.get(i).getName().toUpperCase()
//                            .contains(constraint.toString().toUpperCase())) {
//                        nPlanetList.add(PeriodActivityItems.get(i));
//
//                    }
//                }
//
//                results.values = nPlanetList;
//                results.count = nPlanetList.size();
//
//            }
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            // Now we have to inform the adapter about the new list filtered
            if (results.count == 0)
                notifyDataSetInvalidated();
            else {

                PeriodActivityItems = (ArrayList<PeriodActivityItem>) results.values;
                notifyDataSetChanged();
            }

        }

    }
}
