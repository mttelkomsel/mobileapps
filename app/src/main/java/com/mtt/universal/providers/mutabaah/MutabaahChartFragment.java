package com.mtt.universal.providers.mutabaah;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.mtt.universal.Config;
import com.mtt.universal.R;
import com.mtt.universal.model.ActivityItem;
import com.mtt.universal.model.DayAxisValueFormatter;
import com.mtt.universal.model.MyAxisValueFormatter;
import com.mtt.universal.model.PeriodActivityItem;
import com.mtt.universal.model.XYMarkerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.mtt.universal.MainActivity.FRAGMENT_DATA;
import static com.mtt.universal.MainActivity.mDbHelper;
import static com.mtt.universal.MainActivity.listActivityChart;
import static com.mtt.universal.MainActivity.listHistoryChart;
import static com.mtt.universal.MainActivity.mChart;
import static com.mtt.universal.MainActivity.myAlert;

/**
 * Created by Dimas on 5/21/2017.
 */

public class MutabaahChartFragment extends Fragment {
    private static Activity mAct;
    private LinearLayout ll;
    private String url;
    //protected BarChart mChart;
    private static ArrayList<String> listPeriod = new ArrayList<String>();
    private Button buttonDari;private Calendar dateSelectedDari, dateSelectedSampai;
    private Button buttonSampai;

    private Spinner activitySpinner;

    private SimpleDateFormat dateFormatter, PeriodDateFormatted, datetimeFormatted;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mAct = getActivity();
        url = MutabaahChartFragment.this.getArguments().getStringArray(FRAGMENT_DATA)[0];

        ll = (LinearLayout) inflater.inflate(R.layout.fragment_mutabaah_chart, container, false);
        mChart = (BarChart) ll.findViewById(R.id.chart1);
        mChart.invalidate();
        buttonDari = (Button) ll.findViewById(R.id.set_dari);
        buttonSampai = (Button) ll.findViewById(R.id.set_sampai);
        activitySpinner = (Spinner)  ll.findViewById(R.id.activity_spinner);
        activitySpinner.setId(0);

        activitySpinner.setOnItemSelectedListener((new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setData(activitySpinner.getSelectedItem().toString());
                mChart.invalidate();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                // sometimes you need nothing here
            }
        }));

        setInitialChart();


        dateSelectedDari = Calendar.getInstance();
        dateSelectedDari.add(Calendar.DATE, -30);

        dateSelectedSampai = Calendar.getInstance();

        dateFormatter = new SimpleDateFormat("dd MMM yyyy", Locale.US);

        buttonDari.setText(dateFormatter.format(dateSelectedDari.getTime()));
        buttonDari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDateTimeField(buttonDari, dateSelectedDari);

            }
        });

        buttonSampai.setText(dateFormatter.format(dateSelectedSampai.getTime()));
        buttonSampai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDateTimeField(buttonSampai, dateSelectedSampai);

            }
        });

        setData(Config.SHALAT);

        return ll;
    }

    private void setInitialChart(){
        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);

        mChart.getDescription().setEnabled(false);
        mChart.setMaxVisibleValueCount(32);

        mChart.setPinchZoom(false);

        mChart.setDrawGridBackground(false);

        IAxisValueFormatter xAxisFormatter = new DayAxisValueFormatter(mChart);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);

        IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = mChart.getAxisLeft();
        //leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(10f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        //leftAxis.setAxisMaximum(100f);

        mChart.getAxisRight().setEnabled(false);

//        YAxis rightAxis = mChart.getAxisRight();
//        rightAxis.setDrawGridLines(false);
//        rightAxis.setLabelCount(8, false);
//        rightAxis.setValueFormatter(custom);
//        rightAxis.setSpaceTop(15f);
//        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);

        if(getActivity()!=null) {
            XYMarkerView mv = new XYMarkerView(getActivity(), xAxisFormatter);
            mv.setChartView(mChart); // For bounds control
            mChart.setMarker(mv); // Set the marker to the chart
        }

    }

    public void setData(String type) {

        float start = 32f;

        listPeriod.clear();
        listActivityChart.clear();
        listHistoryChart.clear();

        listActivityChart = mDbHelper.getAllActivity();

        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

//        for (int i = (int) start; i < start + count + 1; i++) {
//            float mult = (range + 1);
//            float val = (float) (Math.random() * mult);
//            yVals1.add(new BarEntry(i, val));
//        }

        listActivityChart = mDbHelper.getAllActivity();
        // get all period
        for (int i = 0; i < listActivityChart.size(); i++) {
            if (i == 0)
                listPeriod.add(listActivityChart.get(i).getPeriod());
            else {
                if (!listActivityChart.get(i - 1).getPeriod().equals(listActivityChart.get(i).getPeriod()))
                    listPeriod.add(listActivityChart.get(i).getPeriod());
            }
        }
        // get all history based on listPeriod and listActivity
        for (String period : listPeriod) {
            PeriodActivityItem history = new PeriodActivityItem();
            int countHistory = 0;
            history.setPeriod(period);

            for (ActivityItem item : listActivityChart) {
                if (item.getPeriod().equals(period)) {
                    history.getItems().add(item);

                    countHistory++;
                }
            }
            history.setTotal(countHistory);
            listHistoryChart.add(history);
        }

        for (int i = 0; i < listHistoryChart.size(); i++) {
            float getPercentage = 20f;
            int totalCount = 0;
            int totalDid = 0;
            for (int j = 0; j < listHistoryChart.get(i).getItems().size(); j++) {
                Log.d("Chart" , listHistoryChart.get(i).getItems().get(j).getType() +  "+"  + listHistoryChart.get(i).getItems().get(j).getResult());
                if(type.equals(Config.SEMUA)) {
                    if (listHistoryChart.get(i).getItems().get(j).getType().equals("checkBox")) {
                        totalCount++;
                        if (listHistoryChart.get(i).getItems().get(j).getResult().equals("1")) {
                            totalDid++;
                        }
                    }
                    getPercentage = (float)totalDid/totalCount;
                    Log.d("Chart" , totalCount +  "/"  + totalDid + "=" + getPercentage);

                }
                else {
                    if (listHistoryChart.get(i).getItems().get(j).getTitle().equals(type)) {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        Date strDate = new Date();
                        try {
                            strDate = sdf.parse(listHistoryChart.get(i).getItems().get(j).getPeriod());
                        }catch (java.text.ParseException e){
                            e.printStackTrace();
                        }

                        if (new Date().after(strDate)) {
                           // catalog_outdated = 1;
                        }
                        SimpleDateFormat getDateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                        Log.d("Chart","Compare : " + listHistoryChart.get(i).getItems().get(j).getPeriod() + " : " + getDateFormatter.format(dateSelectedDari.getTime())
                        + " + " + getDateFormatter.format(dateSelectedSampai.getTime()));
                        if(!listHistoryChart.get(i).getItems().get(j).getResult().replaceAll("[\\D]","").equals(""))
                        {
                            getPercentage = Integer.parseInt(listHistoryChart.get(i).getItems().get(j).getResult().replaceAll("[\\D]", ""));
                        }else{
                            getPercentage = 0;
                        }

                        boolean isSameDay = listHistoryChart.get(i).getItems().get(j).getPeriod().equals(getDateFormatter.format(dateSelectedDari.getTime())) ||
                                listHistoryChart.get(i).getItems().get(j).getPeriod().equals(getDateFormatter.format(dateSelectedSampai.getTime()));

                        if(((strDate.compareTo(dateSelectedDari.getTime()) > 0) &&
                                (strDate.compareTo(dateSelectedSampai.getTime()) < 0)) || isSameDay) {
                            yVals1.add(new BarEntry(i, getPercentage));
                        }
                        Log.d("Chart" , getPercentage +  ":"  + listHistoryChart.get(i).getItems().get(j).getTitle() + ":" + type
                         + ":" + strDate.compareTo(dateSelectedDari.getTime()) + ":" + strDate.compareTo(dateSelectedSampai.getTime()));
                    }

                }
            }


        }

        BarDataSet set1;


        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVals1, "Mutabaah");

            set1.setDrawIcons(false);

            set1.setColors(ColorTemplate.MATERIAL_COLORS);

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setBarWidth(0.9f);

            mChart.setData(data);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
            mChart.invalidate();
        }
    }

    private void setDateTimeField(final Button button,final Calendar getDate) {
        Calendar newCalendar = getDate;

        new DatePickerDialog(mAct, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                getDate.set(year, monthOfYear, dayOfMonth, 0, 0);
                //Toast.makeText(getActivity(), dateFormatter.format(dateSelected.getTime()), Toast.LENGTH_SHORT).show();
                button.setText(dateFormatter.format(getDate.getTime()));
                setData(activitySpinner.getSelectedItem().toString());
                mChart.invalidate();
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

}
