package com.mtt.universal.providers.mutabaah;

/**
 * Created by airgalon on 5/14/2017.
 */

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mtt.universal.R;
import com.mtt.universal.model.ActivityItem;
import com.mtt.universal.providers.rss.RSSFeed;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import static com.mtt.universal.MainActivity.FRAGMENT_DATA;
import static com.mtt.universal.MainActivity.mDbHelper;
import static com.mtt.universal.MainActivity.myAlert;

/**
 * This activity is used to display a list of rss items
 */

public class MutabaahTodayFragment extends Fragment {

    private RSSFeed myRssFeed = null;
    private Activity mAct;
    private LinearLayout ll;
    private String url;
    private RelativeLayout pDialog;
    private Button bDatePicker, bTambah, bHapus, bSimpan;
    private LinearLayout llItem;
    private Calendar dateSelected;
    private SimpleDateFormat dateFormatter, PeriodDateFormatted, datetimeFormatted;
    private DatePickerDialog datePickerDialog;
    private ArrayList<LinearLayout> listUserDefined = new ArrayList<LinearLayout>();

    //template activitas
    private Spinner sShalat;
    private EditText etTilawah;
    private CheckBox cbDuha;
    private CheckBox cbLail;
    private CheckBox cbPuasa;
    private CheckBox cbSedekah;
    private CheckBox cbOlahraga;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mAct = getActivity();


        url = MutabaahTodayFragment.this.getArguments().getStringArray(FRAGMENT_DATA)[0];

        dateSelected = Calendar.getInstance();
        dateFormatter = new SimpleDateFormat("EEEE, dd MMM yyyy", Locale.US);
        PeriodDateFormatted = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        datetimeFormatted = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);

        ll = (LinearLayout) inflater.inflate(R.layout.fragment_mutabaah_today, container, false);

        // template aktivitas
        sShalat = (Spinner) ll.findViewById(R.id.spinner1);
        etTilawah = (EditText) ll.findViewById(R.id.editText1);
        cbDuha = (CheckBox) ll.findViewById(R.id.checkBox1);
        cbLail = (CheckBox) ll.findViewById(R.id.checkBox2);
        cbPuasa = (CheckBox) ll.findViewById(R.id.checkBox3);
        cbSedekah = (CheckBox) ll.findViewById(R.id.checkBox4);
        cbOlahraga = (CheckBox) ll.findViewById(R.id.checkBox5);

        //setHasOptionsMenu(true);
        bDatePicker = (Button) ll.findViewById(R.id.button1);
        bTambah = (Button) ll.findViewById(R.id.button2);
        bHapus = (Button) ll.findViewById(R.id.button3);
        bSimpan = (Button) ll.findViewById(R.id.button4);
        llItem = (LinearLayout) ll.findViewById(R.id.linearLayout1);

        bDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDateTimeField();
            }
        });
        bDatePicker.setText(dateFormatter.format(dateSelected.getTime()));

        bTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAlert.showAlertAddActivity(listUserDefined, llItem);
//                for(int i=0;i<listUserDefined.size();i++){
//                    TextView tv = (TextView) listUserDefined.get(i).findViewById(R.id.textView1);
//                    Toast.makeText(getActivity(), tv.getText().toString(), Toast.LENGTH_SHORT).show();
//                }
            }
        });

        bHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listUserDefined.size() > 0) {
                    LinearLayout lastView = listUserDefined.get(listUserDefined.size() - 1);

                    llItem.removeView(lastView);
                    listUserDefined.remove(listUserDefined.size() - 1);

                } else {
                    Toast.makeText(mAct, "Aktivitas tambahan tidak ditemukan", Toast.LENGTH_SHORT).show();
                }
            }
        });

        bSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Toast.makeText(mAct, dbDateFormatted.format(dateSelected.getTime()), Toast.LENGTH_SHORT).show();
                insertData();

//                activity.setPeriod(PeriodDateFormatted.format(dateSelected.getTime()));
//                activity.setTitle();
//
//                activity.setCreatedTime(datetimeFormatted.format(now.getTime()));
//                activity.setModifiedTime("");
            }
        });

        //test delete
        //mDbHelper.deleteActivity(mAct, "20-05-2017");

        //test view
//        for (ActivityItem activity : mDbHelper.getAllActivity()) {
//            Toast.makeText(mAct, activity.getPeriod(), Toast.LENGTH_SHORT).show();
//        }

        //Toast.makeText(mAct, url, Toast.LENGTH_SHORT).show();

        return ll;
    }

    public void insertData() {
        Calendar now = Calendar.getInstance();
        ActivityItem activity = null;
        ArrayList<ActivityItem> activites = new ArrayList<ActivityItem>();

        activity = new ActivityItem(
                PeriodDateFormatted.format(dateSelected.getTime()),
                "Shalat awal waktu",
                sShalat.getSelectedItem().toString(),
                "spinner",
                datetimeFormatted.format(now.getTime()),
                ""
        );
        activites.add(activity);

        activity = new ActivityItem(
                PeriodDateFormatted.format(dateSelected.getTime()),
                "Tilawah",
                etTilawah.getText().toString(),
                "editText",
                datetimeFormatted.format(now.getTime()),
                ""
        );
        activites.add(activity);

        activity = new ActivityItem(
                PeriodDateFormatted.format(dateSelected.getTime()),
                "Shalat duha",
                (cbDuha.isChecked()) ? "1" : "0",
                "checkBox",
                datetimeFormatted.format(now.getTime()),
                ""
        );
        activites.add(activity);

        activity = new ActivityItem(
                PeriodDateFormatted.format(dateSelected.getTime()),
                "Qiyamul Lail",
                (cbLail.isChecked()) ? "1" : "0",
                "checkBox",
                datetimeFormatted.format(now.getTime()),
                ""
        );
        activites.add(activity);

        activity = new ActivityItem(
                PeriodDateFormatted.format(dateSelected.getTime()),
                "Puasa",
                (cbPuasa.isChecked()) ? "1" : "0",
                "checkBox",
                datetimeFormatted.format(now.getTime()),
                ""
        );
        activites.add(activity);

        activity = new ActivityItem(
                PeriodDateFormatted.format(dateSelected.getTime()),
                "Sedekah",
                (cbSedekah.isChecked()) ? "1" : "0",
                "checkBox",
                datetimeFormatted.format(now.getTime()),
                ""
        );
        activites.add(activity);

        activity = new ActivityItem(
                PeriodDateFormatted.format(dateSelected.getTime()),
                "Olahraga",
                (cbOlahraga.isChecked()) ? "1" : "0",
                "checkBox",
                datetimeFormatted.format(now.getTime()),
                ""
        );
        activites.add(activity);

        for (LinearLayout layout : listUserDefined) {
            String title = ((TextView) layout.findViewById(R.id.item1)).getText().toString();
            String type = "";
            String result = "";

            View viewType = layout.findViewById(R.id.item2);
            if (viewType instanceof CheckBox) {
                type = "checkBox";
                result = ((CheckBox) viewType).isChecked() ? "1" : "0";
            } else if (viewType instanceof EditText) {
                type = "editText";
                result = ((EditText) viewType).getText().toString();
            }

            activity = new ActivityItem(
                    PeriodDateFormatted.format(dateSelected.getTime()),
                    title,
                    result,
                    type,
                    datetimeFormatted.format(now.getTime()),
                    ""
            );
            activites.add(activity);

        }

        mDbHelper.insertActivities(mAct, activites);


    }

    private void setDateTimeField() {
        Calendar newCalendar = dateSelected;

        new DatePickerDialog(mAct, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dateSelected.set(year, monthOfYear, dayOfMonth, 0, 0);
                //Toast.makeText(getActivity(), dateFormatter.format(dateSelected.getTime()), Toast.LENGTH_SHORT).show();
                bDatePicker.setText(dateFormatter.format(dateSelected.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }






}
