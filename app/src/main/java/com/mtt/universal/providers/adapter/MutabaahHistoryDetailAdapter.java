package com.mtt.universal.providers.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.mtt.universal.R;
import com.mtt.universal.model.ActivityItem;

import java.util.ArrayList;
import java.util.List;


public class MutabaahHistoryDetailAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<ActivityItem> ActivityItems;
    private List<ActivityItem> OriActivityItems;

    private Filter planetFilter;

    public MutabaahHistoryDetailAdapter(Context context, List<ActivityItem> ActivityItems) {
        this.context = context;
        this.ActivityItems = ActivityItems;
        this.OriActivityItems = ActivityItems;
    }

    public void resetData() {
        this.ActivityItems = this.OriActivityItems;
    }

    public int getCount() {
        return ActivityItems.size();
    }

    public ActivityItem getItem(int position) {
        return ActivityItems.get(position);
    }

    private class ViewHolder {

        TextView tvNo, tvTitle, tvResult;
        //ImageView ivIcon;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;


        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater
                    .inflate(R.layout.alertdialog_mutabaahhistory_detail, parent, false);

            viewHolder = new ViewHolder();

            viewHolder.tvNo = (TextView) convertView
                    .findViewById(R.id.textView1);
            viewHolder.tvTitle = (TextView) convertView
                    .findViewById(R.id.textView2);
            viewHolder.tvResult = (TextView) convertView
                    .findViewById(R.id.textView3);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final ActivityItem row = ActivityItems.get(position);

        viewHolder.tvNo.setText(Integer.toString(position + 1) + ". ");
        viewHolder.tvTitle.setText(row.getTitle());

        String result = "";
        switch (row.getType()) {
            case "checkBox":
                result = row.getResult().equals("1") ? "Ya" : "Tidak";
                break;
            case "editText":
                result = row.getResult().equals("") ? "Belum diisi" : row.getResult();
                break;
            case "spinner":
                result = row.getResult() + " kali";
                break;
        }

        viewHolder.tvResult.setText(result);


//
//        viewHolder.ivIcon.setImageResource(row.getImg());

        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return ActivityItems.indexOf(getItem(position));
    }

    @Override
    public Filter getFilter() {
        if (planetFilter == null)
            planetFilter = new PlanetFilter();

        return planetFilter;
    }

    private class PlanetFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
//            if (constraint == null || constraint.length() == 0) {
//                // No filter implemented we return all the list
//                results.values = OriActivityItems;
//                results.count = OriActivityItems.size();
//            } else {
//                // We perform filtering operation
//                List<ActivityItem> nPlanetList = new ArrayList<ActivityItem>();
//
//                for (int i = 0; i < ActivityItems.size(); i++) {
//                    if (ActivityItems.get(i).getName().toUpperCase()
//                            .contains(constraint.toString().toUpperCase())) {
//                        nPlanetList.add(ActivityItems.get(i));
//
//                    }
//                }
//
//                results.values = nPlanetList;
//                results.count = nPlanetList.size();
//
//            }
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            // Now we have to inform the adapter about the new list filtered
            if (results.count == 0)
                notifyDataSetInvalidated();
            else {

                ActivityItems = (ArrayList<ActivityItem>) results.values;
                notifyDataSetChanged();
            }

        }

    }
}
