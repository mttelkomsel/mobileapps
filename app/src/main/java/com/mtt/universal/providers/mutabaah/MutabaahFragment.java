package com.mtt.universal.providers.mutabaah;

/**
 * Created by airgalon on 5/14/2017.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mtt.universal.MainActivity;
import com.mtt.universal.R;
import com.mtt.universal.providers.rss.RSSFeed;
import com.mtt.universal.providers.rss.RSSItem;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * This activity is used to display a list of rss items
 */

public class MutabaahFragment extends Fragment {

    private RSSFeed myRssFeed = null;
    private Activity mAct;
    private LinearLayout ll;
    private String url;
    private RelativeLayout pDialog;
    private Button bDatePicker, bTambah, bHapus, bSimpan;
    private LinearLayout llItem;
    private Calendar dateSelected;
    private SimpleDateFormat dateFormatter;
    private DatePickerDialog datePickerDialog;
    private ArrayList<LinearLayout> listUserDefined = new ArrayList<LinearLayout>();

    public class RssAdapter extends ArrayAdapter<RSSItem> {

        public RssAdapter(Context context, int textViewResourceId,
                          List<RSSItem> list) {
            super(context, textViewResourceId, list);
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;

            final ViewHolder holder;

            if (row == null) {

                LayoutInflater inflater = mAct.getLayoutInflater();
                row = inflater.inflate(R.layout.fragment_rss_row, null);

                holder = new ViewHolder();

                holder.listTitle = (TextView) row.findViewById(R.id.listtitle);
                holder.listPubdate = (TextView) row.findViewById(R.id.listpubdate);
                holder.listDescription = (TextView) row.findViewById(R.id.shortdescription);
                holder.listThumb = (ImageView) row.findViewById(R.id.listthumb);

                row.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.listTitle.setText(myRssFeed.getList().get(position).getTitle());
            holder.listPubdate.setText(myRssFeed.getList().get(position).getPubdate());

            String html = myRssFeed.getList().get(position).getRowDescription();

            holder.listDescription.setText(html);

            holder.listThumb.setImageDrawable(null);

            String thumburl = myRssFeed.getList().get(position).getThumburl();
            if (thumburl != null && !thumburl.equals("")) {
                //setting the image
                final ImageView imageView = holder.listThumb; // The view Picasso is loading an image into
                final Target target = new Target() {
                    @Override
                    public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                                    /* Save the bitmap or do something with it here */

                        if (10 > bitmap.getWidth() || 10 > bitmap.getHeight()) {
                            // handle scaling
                            holder.listThumb.setVisibility(View.GONE);
                        } else {
                            holder.listThumb.setVisibility(View.VISIBLE);
                            holder.listThumb.setImageBitmap(bitmap);
                        }
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                    }
                };

                imageView.setTag(target);

                Picasso.with(mAct)
                        .load(myRssFeed.getList().get(position).getThumburl())
                        .into(target);
            } else {
                holder.listThumb.setVisibility(View.GONE);
            }

            return row;
        }

    }

    static class ViewHolder {
        TextView listTitle;
        TextView listPubdate;
        TextView listDescription;
        ImageView listThumb;
        int position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dateSelected = Calendar.getInstance();
        dateFormatter = new SimpleDateFormat("EEEE, dd MMM yyyy", Locale.US);

        ll = (LinearLayout) inflater.inflate(R.layout.fragment_mutabaah, container, false);

        //setHasOptionsMenu(true);
        bDatePicker = (Button) ll.findViewById(R.id.button1);
        bTambah = (Button) ll.findViewById(R.id.button2);
        bHapus = (Button) ll.findViewById(R.id.button3);
        bSimpan = (Button) ll.findViewById(R.id.button4);
        llItem = (LinearLayout) ll.findViewById(R.id.linearLayout1);

        bDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDateTimeField();
            }
        });
        bDatePicker.setText(dateFormatter.format(dateSelected.getTime()));

        bTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertAddActivity();
//                for(int i=0;i<listUserDefined.size();i++){
//                    TextView tv = (TextView) listUserDefined.get(i).findViewById(R.id.textView1);
//                    Toast.makeText(getActivity(), tv.getText().toString(), Toast.LENGTH_SHORT).show();
//                }
            }
        });

        bHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listUserDefined.size() > 0) {
                    LinearLayout lastView = listUserDefined.get(listUserDefined.size() - 1);

                    llItem.removeView(lastView);
                    listUserDefined.remove(listUserDefined.size() - 1);

                } else {
                    Toast.makeText(mAct, "Aktivitas tambahan tidak ditemukan", Toast.LENGTH_SHORT).show();
                }
            }
        });

        bSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return ll;
    }

    private void setDateTimeField() {
        Calendar newCalendar = dateSelected;

        new DatePickerDialog(mAct, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dateSelected.set(year, monthOfYear, dayOfMonth, 0, 0);
                //Toast.makeText(getActivity(), dateFormatter.format(dateSelected.getTime()), Toast.LENGTH_SHORT).show();
                bDatePicker.setText(dateFormatter.format(dateSelected.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAct = getActivity();

        url = MutabaahFragment.this.getArguments().getStringArray(MainActivity.FRAGMENT_DATA)[0];

        Toast.makeText(mAct, url, Toast.LENGTH_SHORT).show();
    }


    public void showAlertAddActivity() {
        LayoutInflater inflater = LayoutInflater.from(mAct);
        View dialogview = inflater.inflate(R.layout.alertdialog_add_activity,
                null);
        final AlertDialog alert = new AlertDialog.Builder(mAct)
                .create();
        alert.setTitle("Tambah Aktivitas");
        alert.setView(dialogview);

        final EditText etJudul = (EditText) dialogview.findViewById(R.id.editText1);
        final Spinner sJenis = (Spinner) dialogview
                .findViewById(R.id.spinner1);
        Button bTambah = (Button) dialogview.findViewById(R.id.button1);

        bTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mAct, etJudul.getText().toString()+"\n"+sJenis.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                LayoutInflater inflater = LayoutInflater.from(mAct);
                LinearLayout newLayout = null;

                if (sJenis.getSelectedItem().toString().equals("TextBox")) {
                    newLayout = (LinearLayout) inflater.inflate(R.layout.item_mutabaah_edittext, null, false);
                } else if (sJenis.getSelectedItem().toString().equals("Ceklist")) {
                    newLayout = (LinearLayout) inflater.inflate(R.layout.item_mutabaah_checkbox, null, false);
                }

                TextView tvJudul = (TextView) newLayout.findViewById(R.id.textView1);
                tvJudul.setText(etJudul.getText().toString());

                listUserDefined.add(newLayout);
                llItem.addView(newLayout);

                alert.dismiss();
            }
        });

        alert.show();
    }

}
