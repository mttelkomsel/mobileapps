package com.mtt.universal.providers.zis;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.mtt.universal.MainActivity;
import com.mtt.universal.R;
import com.mtt.universal.util.Helper;
import com.mtt.universal.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * This fragment is used to display a list of facebook posts
 */

public class ZisFragment extends Fragment {

	//private ArrayList<ZisItem> postList = null;
	private ListView listView = null;
	private View footerView;
	private Activity mAct;
	private DownloadFilesTask mTask;
	private ZisAdapter postListAdapter = null;

	private RelativeLayout ll;
	RelativeLayout dialogLayout;

	String nextpageurl;

	String username;

	Boolean isLoading = false;

	private static String API_URL_BEGIN = "http://apps.mtt.or.id/zis.php";

	private static final String LOG_TAG = "iabv3";

	// PRODUCT & SUBSCRIPTION IDS
	/*private String PRODUCT_ID = "com.mtt.universal.5k";
	private static final String PRODUCT_ID_5K = "com.mtt.universal.5k";
	private static final String PRODUCT_ID_10K = "com.mtt.universal.10k";
	private static final String PRODUCT_ID_25K = "com.mtt.universal.25k";
	private static final String PRODUCT_ID_50K = "com.mtt.universal.50k";
	private static final String PRODUCT_ID_100K = "com.mtt.universal.100k";
	private static final String LICENSE_KEY = null; // PUT YOUR MERCHANT KEY HERE;

	private static final String MERCHANT_ID=null;
*/
	private BillingProcessor bp;
	private boolean readyToPurchase = false;

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ll = (RelativeLayout) inflater.inflate(R.layout.fragment_list,
				container, false);
		setHasOptionsMenu(true);

		username = this.getArguments().getStringArray(MainActivity.FRAGMENT_DATA)[0];

		footerView = inflater.inflate(R.layout.listview_footer, null);
		listView = (ListView) ll.findViewById(R.id.list);
		listView.setOnScrollListener(new OnScrollListener() {
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {

				if (postListAdapter == null)
					return;

				if (postListAdapter.getCount() == 0)
					return;

				int l = visibleItemCount + firstVisibleItem;
				if (l >= totalItemCount && !isLoading && nextpageurl != null) {
					mTask = new DownloadFilesTask(false);
					mTask.execute();
				}
			}

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {

			}
		});

		/*if(!BillingProcessor.isIabServiceAvailable(mAct)) {
			showToast("In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16");
		}

		bp = new BillingProcessor(mAct, LICENSE_KEY, MERCHANT_ID, new BillingProcessor.IBillingHandler() {
			@Override
			public void onProductPurchased(String productId, TransactionDetails details) {
				showToast("onProductPurchased: " + productId);
				//updateTextViews();
			}

			@Override
			public void onBillingError(int errorCode, Throwable error) {
				showToast("onBillingError: " + Integer.toString(errorCode));
			}
			@Override
			public void onBillingInitialized() {
				showToast("onBillingInitialized");
				readyToPurchase = true;
				//updateTextViews();
			}
			@Override
			public void onPurchaseHistoryRestored() {
				showToast("onPurchaseHistoryRestored");
				for(String sku : bp.listOwnedProducts())
					android.util.Log.d(LOG_TAG, "Owned Managed Product: " + sku);
				for(String sku : bp.listOwnedSubscriptions())
					android.util.Log.d(LOG_TAG, "Owned Subscription: " + sku);
				//updateTextViews();
			}
		});*/

		return ll;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mAct = getActivity();

		mTask = new DownloadFilesTask(true);
		mTask.execute();
	}


	public void updateList(ArrayList<ZisItem> posts, boolean initialload) {
		if (initialload) {
			postListAdapter = new ZisAdapter(mAct, posts, ZisFragment.this);
			listView.setAdapter(postListAdapter);
		} else {
			postListAdapter.addAll(posts);
			postListAdapter.notifyDataSetChanged();
		}
	}

	private class DownloadFilesTask extends AsyncTask<String, Integer, ArrayList<ZisItem>> {

		boolean initialload;

		DownloadFilesTask(boolean firstload) {
			this.initialload = firstload;
		}

		@Override
		protected void onPreExecute() {
			if (isLoading) {
				this.cancel(true);
			} else {
				isLoading = true;
			}
			if (initialload) {
				dialogLayout = (RelativeLayout) ll
						.findViewById(R.id.progressBarHolder);

				if (dialogLayout.getVisibility() == View.GONE) {
					dialogLayout.setVisibility(View.VISIBLE);
					listView.setVisibility(View.GONE);
				}

				//nextpageurl = (API_URL_BEGIN + username + API_URL_MIDDLE  + getResources().getString(R.string.facebook_access_token) + API_URL_END);
				nextpageurl = API_URL_BEGIN;
				if (null != listView) {
					listView.setAdapter(null);
				}
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
					listView.addFooterView(footerView);
				}
			} else {
				listView.addFooterView(footerView);
			}
		}

		@Override
		protected void onPostExecute(ArrayList<ZisItem> posts) {
			if(getActivity()!=null) {
				if (null != posts && posts.size() > 0) {
					updateList(posts, initialload);
				} else if (posts == null) {
					String token = getResources().getString(R.string.facebook_access_token);
					String message = null;
					if (token.equals("YOURFACEBOOKTOKENHERE") || token.equals("")) {
						message = "Debug info: You have not entered a (valid) ACCESS token.";
					}
					Helper.noConnection(mAct, message);
				}

				if (dialogLayout.getVisibility() == View.VISIBLE) {
					dialogLayout.setVisibility(View.GONE);
					Helper.revealView(listView, ll);

					if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
						listView.removeFooterView(footerView);
					}
				} else {
					listView.removeFooterView(footerView);
				}
				isLoading = false;
			}
		}

		@Override
		protected ArrayList<ZisItem> doInBackground(String... params) {
			JSONObject json = Helper.getJSONObjectFromUrl(nextpageurl);
			return parseJson(json);
		}
	}


	public ArrayList<ZisItem> parseJson(JSONObject json) {
		ArrayList<ZisItem> postList = new ArrayList<ZisItem>();
		
		try {
			if (json.has("paging") && json.getJSONObject("paging").has("next"))
				nextpageurl = json.getJSONObject("paging").getString("next");
			else
				nextpageurl = null;

			// parsing json object
			 JSONArray dataJsonArray = json.getJSONArray("data");
             for (int i = 0; i < dataJsonArray.length(); i++) {
            	 try {
                 JSONObject photoJson = dataJsonArray.getJSONObject(i);
                 ZisItem post = new ZisItem();
                 post.id = photoJson.getString("id");
                 post.type = photoJson.getString("type");
                 post.username = photoJson.getString("title");
                 post.profilePhotoUrl = "http://apps.mtt.or.id/images/" + photoJson.getString("image");
                 post.createdTime = new Date(photoJson.getLong("created_time") * 1000);

                 if (photoJson.has("link"))
                	 post.link =  photoJson.getString("link");
                 else 
                	 post.link = "https://mtt.or.id/apps/" + post.id;
                 
                 if (post.type.equals("video")) {
                     post.videoUrl = photoJson.getString("source");
                 }

			/*	 if(post.type.equals("googlepay")){

				 }*/
                 
                 if (photoJson.has("message")){
                	 post.caption = photoJson.getString("message");
                 } else if (photoJson.has("subtitle")){
                	 post.caption = photoJson.getString("subtitle");
                 } else {
                	 post.caption = "";
                 }
                 
                 if (photoJson.has("photo")){
                	 post.imageUrl = photoJson.getString("photo");
                 }
                 

                 // Add to array list
                 postList.add(post);
            	 } catch (Exception e) {
         			Log.e("INFO", "Item " + i +" skipped because of exception");
         			Log.printStackTrace(e);
         		}
			}

			return postList;
		} catch (Exception e) {
			Log.printStackTrace(e);

			return null;
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.refresh_menu, menu);

	}

	@Override
	public void onDestroy() {
		if (bp != null)
			bp.release();
		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.refresh:
			if (!isLoading) {
				new DownloadFilesTask(true).execute();
			} else {
				Toast.makeText(mAct, getString(R.string.already_loading),Toast.LENGTH_LONG).show();
			}
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	public void donate(String PRODUCT_ID ){
		((MainActivity)mAct).donate(PRODUCT_ID);
	}
	public void call(String number){
		((MainActivity)mAct).callNumber(number);
	}

	/*public void showToast(String msg){
		Toast.makeText(mAct, getString(R.string.already_loading),Toast.LENGTH_LONG).show();

	}
	public void donate(String PRODUCT_ID ){
		if (!readyToPurchase) {
			showToast("Billing not initialized.");
			return;
		}

		bp.purchase(mAct,PRODUCT_ID);
		Boolean consumed = bp.consumePurchase(PRODUCT_ID);
		if (consumed)showToast("Jazakallah Khairan Katsiran, donasi berhasil dilakukan..");
	}

	public void call(String number){
		try {
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			callIntent.setData(Uri.parse("tel:"+number));
			mAct.startActivity(callIntent);

		} catch (ActivityNotFoundException activityException) {
			android.util.Log.e("Calling a Phone Number", "Call failed", activityException);
		}
	}*/
}
