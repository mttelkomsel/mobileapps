package com.mtt.universal.model;

import android.provider.BaseColumns;

/**
 * Created by emirdon on 5/20/2017.
 */

public final class MutabaahContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private MutabaahContract() {
    }

    /* Inner class that defines the table contents */
    public static class MutabaahActivity implements BaseColumns {
        public static final String TABLE_NAME = "activity";
        public static final String COLUMN_NAME_PERIOD = "period";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_RESULT = "result";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final String COLUMN_NAME_CREATED_TIME = "created_time";
        public static final String COLUMN_NAME_MODIFIED_TIME = "modified_time";
    }

    public static final String SQL_CREATE_ACTIVITY =
            "CREATE TABLE " + MutabaahActivity.TABLE_NAME + " (" +
                    MutabaahActivity._ID + " INTEGER PRIMARY KEY," +
                    MutabaahActivity.COLUMN_NAME_PERIOD + " TEXT," +
                    MutabaahActivity.COLUMN_NAME_TITLE + " TEXT," +
                    MutabaahActivity.COLUMN_NAME_RESULT + " TEXT," +
                    MutabaahActivity.COLUMN_NAME_TYPE + " TEXT," +
                    MutabaahActivity.COLUMN_NAME_CREATED_TIME + " TEXT," +
                    MutabaahActivity.COLUMN_NAME_MODIFIED_TIME + " TEXT)";

    public static final String SQL_DELETE_ACTIVITY =
            "DROP TABLE IF EXISTS " + MutabaahActivity.TABLE_NAME;
}
