package com.mtt.universal.model;

import java.util.ArrayList;

/**
 * Created by emirdon on 5/20/2017.
 */

public class PeriodActivityItem {
    private String period;
    private ArrayList<ActivityItem> items = new ArrayList<ActivityItem>();
    private int total;

    public PeriodActivityItem() {
    }

    public PeriodActivityItem(String period, ArrayList<ActivityItem> items, int total) {

        this.period = period;
        this.items = items;
        this.total = total;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public ArrayList<ActivityItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<ActivityItem> items) {
        this.items = items;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }


}
