package com.mtt.universal.model;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import static com.mtt.universal.MainActivity.listHistoryChart;

public class DayAxisValueFormatter implements IAxisValueFormatter
{


    private BarLineChartBase<?> chart;

    public DayAxisValueFormatter(BarLineChartBase<?> chart) {
        this.chart = chart;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {

        int days = (int) value;


        return listHistoryChart.get(days).getPeriod();


    }


}
