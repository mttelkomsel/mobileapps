package com.mtt.universal.model;

/**
 * Created by emirdon on 5/20/2017.
 */

public class ActivityItem {
    private String period;
    private String title;
    private String result;
    private String type;
    private String createdTime;
    private String modifiedTime;

    public ActivityItem() {
    }

    public ActivityItem(String period, String title, String result, String type, String createdTime, String modifiedTime) {
        this.period = period;
        this.title = title;
        this.result = result;
        this.type = type;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}
