package com.mtt.universal.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.mtt.universal.model.MutabaahContract.MutabaahActivity;

import java.util.ArrayList;

import static com.mtt.universal.MainActivity.mDbHelper;
import static com.mtt.universal.model.MutabaahContract.SQL_CREATE_ACTIVITY;
import static com.mtt.universal.model.MutabaahContract.SQL_DELETE_ACTIVITY;

/**
 * Created by emirdon on 5/20/2017.
 */

public class MutabaahDbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "mutabaah.db";

    public MutabaahDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ACTIVITY);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ACTIVITY);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void insertActivity(Context c, ActivityItem item) {
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(MutabaahActivity.COLUMN_NAME_PERIOD, item.getPeriod());
        values.put(MutabaahActivity.COLUMN_NAME_TITLE, item.getTitle());
        values.put(MutabaahActivity.COLUMN_NAME_RESULT, item.getResult());
        values.put(MutabaahActivity.COLUMN_NAME_TYPE, item.getType());
        values.put(MutabaahActivity.COLUMN_NAME_CREATED_TIME, item.getCreatedTime());
        values.put(MutabaahActivity.COLUMN_NAME_MODIFIED_TIME, item.getModifiedTime());
        // Insert the new row, returning the primary key value of the new row
        db.insert(MutabaahActivity.TABLE_NAME, null, values);

    }

    public void insertActivities(Context c, ArrayList<ActivityItem> items) {
        String flagNotice = "insertActivity";

        if (isExistActivity(c, items.get(0).getPeriod())) {
            deleteActivity(c, items.get(0).getPeriod(), true);
            flagNotice = "updateActivity";
        }

        for (ActivityItem item : items) {
            insertActivity(c, item);
        }

        notice(c, flagNotice, 1);
    }

    public void deleteActivity(Context c, String period, Boolean isUpdate) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        // Define 'where' part of query.
        String selection = MutabaahActivity.COLUMN_NAME_PERIOD + " LIKE ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = {period};
        // Issue SQL statement.
        long newRowId = db.delete(MutabaahActivity.TABLE_NAME, selection, selectionArgs);

        if(!isUpdate)notice(c, "deleteActivity", newRowId);
    }

    public ArrayList<ActivityItem> getAllActivity() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        ArrayList<ActivityItem> result = new ArrayList<ActivityItem>();


        String[] projection = {
                MutabaahActivity._ID,
                MutabaahActivity.COLUMN_NAME_PERIOD,
                MutabaahActivity.COLUMN_NAME_TITLE,
                MutabaahActivity.COLUMN_NAME_RESULT,
                MutabaahActivity.COLUMN_NAME_TYPE,
                MutabaahActivity.COLUMN_NAME_CREATED_TIME,
                MutabaahActivity.COLUMN_NAME_MODIFIED_TIME
        };

        String sortOrder =
                MutabaahActivity.COLUMN_NAME_PERIOD + " ASC";

        Cursor cursor = db.query(
                MutabaahActivity.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        while (cursor.moveToNext()) {
            ActivityItem activity = new ActivityItem(
                    cursor.getString(cursor.getColumnIndexOrThrow(MutabaahActivity.COLUMN_NAME_PERIOD)),
                    cursor.getString(cursor.getColumnIndexOrThrow(MutabaahActivity.COLUMN_NAME_TITLE)),
                    cursor.getString(cursor.getColumnIndexOrThrow(MutabaahActivity.COLUMN_NAME_RESULT)),
                    cursor.getString(cursor.getColumnIndexOrThrow(MutabaahActivity.COLUMN_NAME_TYPE)),
                    cursor.getString(cursor.getColumnIndexOrThrow(MutabaahActivity.COLUMN_NAME_CREATED_TIME)),
                    cursor.getString(cursor.getColumnIndexOrThrow(MutabaahActivity.COLUMN_NAME_MODIFIED_TIME))

            );
            Log.d("db-helper",activity.getCreatedTime() + ":" + activity.getModifiedTime() + ":" + activity.getPeriod() +
                     ":" + activity.getResult() + ":" + activity.getTitle() + ":" + activity.getType());
            result.add(activity);
        }
        cursor.close();

        return result;
    }

    public void notice(Context c, String flag, long value) {
        String result = "Transaction failed";

        switch (flag) {
            case "insertActivity":
                result = (value != -1) ? "Activity has been saved" : result;
                break;
            case "deleteActivity":
                result = (value != -1) ? "Activity has been deleted " : result;
                break;
            case "updateActivity":
                result = (value != -1) ? "Activity has been updated" : result;
                break;
        }

        Toast.makeText(c, result, Toast.LENGTH_SHORT).show();
    }

    public boolean isExistActivity(Context c, String period) {
        boolean isExist = false;
        SQLiteDatabase db = mDbHelper.getReadableDatabase();


        String selection = MutabaahActivity.COLUMN_NAME_PERIOD + " LIKE ?";
        String[] selectionArgs = {period};


        String[] projection = {
                MutabaahActivity._ID
        };

        Cursor cursor = db.query(
                MutabaahActivity.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        while (cursor.moveToNext()) {
            isExist = true;
        }
        cursor.close();

        return isExist;
    }

}
